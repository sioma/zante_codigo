/*  ===============================================================================================
 *  Name        : ZanteVariables.h
 *  Autor       : SIOMA, Diego Escobar
 *  Version     : 5.1
 *  ===============================================================================================
 */

#ifndef ZanteVariables_h
#define ZanteVariables_h

volatile bool aNewInt = false;
volatile bool bNewInt = false;
bool rfidActivado = false;
bool primeraVez = true;
bool alertaBatMedia = false;
bool alertaBaja = false;
bool lostGpsAlarm = false;
bool GpsAlarm = false;
bool jacoConfigured = false;
bool dataTimeFixed = false;
bool ledStatus = false;
bool zanteIdExists = false;
bool firstCharge = true;
bool gpsActivado = false;
bool wifiActivado = false;
bool debugEnable = false;

float voltajeBat = 0.0;
// int porcentajeBat = 0;

unsigned long cont = 0;
unsigned long temporizadorZanteps = 0;
unsigned long temporizadorMuestreoGps = 0;
unsigned long temporizadorInterrupcion = 0;
unsigned long temporizadorAlarmaGps = 0;
unsigned long timeOn = 0;
unsigned long timeOff = 0;
unsigned long tiempoActual = 0;
unsigned long tiempoActual2 = 0;
unsigned long timeSave = 0;
unsigned long tempSave = 60000;
unsigned long debounceStartTime = 0;
unsigned long debounceStartTime_button = 0;

String colorLed = "";
String tagLeidoAnterior[2] = {"", ""};
String tagLeidoMem = "";
String tagLeido[2] = {"", ""};

String datosGps[4];
unsigned int tamanoDatosGps = (sizeof(datosGps) / sizeof(datosGps[0]));

#endif /*ZanteVariables_h*/
