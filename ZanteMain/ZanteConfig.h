
/*  ===============================================================================================
 *  Name        : ZanteConfig.h
 *  Autor       : SIOMA, Diego Escobar
 *  Version     : 5.1
 *  ===============================================================================================
 */

#ifndef ZanteConfig_h
#define ZanteConfig_h

/*Define version del firmware*/
#define CODE_VERSION "5.1"

/*Habilitar modo debug*/
#define _DEBUG_MODE_ 0
// Cambiar a 1 para habilitar el modo debug

#endif /*ZanteConfig_h*/
