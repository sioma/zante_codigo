/*  ===============================================================================================
    Name        : Zante.ino
    Autor       : SIOMA, Diego Escobar
    Version     : 5.1
    ===============================================================================================
*/

#include "ZanteIncludes.h"

void Rfid_int();
void Boton_int();
bool Periodo_muestreo(unsigned long &, unsigned long);
bool Pendiente_por_subir();
void Set_modules_debugger(bool);
void Save_operation(String *, unsigned char);
bool Config_jaco();
void Vaciar_buffer_serial();
void Vaciar_buffer_wifi();
void Guardar_red(String *, String);
void Guardar_red(String, String);
void Print_redes(String *, int);
void Print_msg_1();
void Print_msg_2();
void Print_msg_3();
void Print_msg_4();
void Charging_mode(unsigned long tempSync = 900000);
void Adjust_date_time(String);
bool Guardar_datos(String *, unsigned char);
bool Guardar_datos(String *, String, unsigned char);
void Set_personaID(unsigned long);
void Save_cierre_Dia (String _fecha = "", uint32_t _personaId = 0);
uint16_t Cierre_dia_counter ();

void Sleep_now();

void Show(char value);
void Show(String value);
void Show(double value);
void Show(float value);
void Show(int value);
void Show(byte value);

// ModuloGps
void Init_gps();
void Stop_gps();
bool Fixed_gps();
void Set_gps_filter(bool);
bool Filtered_gps();
bool Get_datos_gps(String *);

// Sensor temperatura
static float Convert_dec_to_frac(uint8_t val);
static float Calculate_temperature(uint16_t TP, uint16_t TC);
float Get_tempc();

// ModuloRFID
void Stop_rfid();
void Init_rfid();
void Receive_rfid();

// ModuloWiFI
bool FTP_protocol(String);
void Program_protocol(String);
String Request_to_server(String, String, String, String);
String Request_to_server(urlTipo, String);
void Sync_cierre_dia ();


void Init_wifi();
void Stop_wifi();
String Get_date_time();
bool Connect_to_wifi();
void Sync_field_data();
void Set_file_tipo(fileTipo);

/*
   Modulo feedbacks
*/
void Activar_alarma(unsigned char, unsigned long);
void Activar_alarma(unsigned char);
void Desactivar_alarmas();
void Activar_indicador(unsigned char, String, unsigned long);
void Activar_indicador(unsigned char, String);
void Activar_indicador(unsigned char, unsigned long);
void Activar_indicador(unsigned char);
void Handle_feedbacks();

Uart wifiSerial(&sercom4, 51, 50, SERCOM_RX_PAD_2, UART_TX_PAD_0); // Se configura un puerto serial para rfid
void SERCOM4_0_Handler()
{
  wifiSerial.IrqHandler();
}
void SERCOM4_1_Handler()
{
  wifiSerial.IrqHandler();
}
void SERCOM4_2_Handler()
{
  wifiSerial.IrqHandler();
}
void SERCOM4_3_Handler()
{
  wifiSerial.IrqHandler();
}

Uart gpsSerial(&sercom1, 11, 13, SERCOM_RX_PAD_3, UART_TX_PAD_0); // se configura puerto serial paa gps
void SERCOM1_0_Handler()
{
  gpsSerial.IrqHandler();
}
void SERCOM1_1_Handler()
{
  gpsSerial.IrqHandler();
}
void SERCOM1_2_Handler()
{
  gpsSerial.IrqHandler();
}
void SERCOM1_3_Handler()
{
  gpsSerial.IrqHandler();
}

enum tipoAlarma
{
  _alarmaLabor,
  _alarmaGps,
  _alarmaPersona,
  _alarmaSync,
  _alarmaMemoria
};

enum tipoIndicador
{
  _configModeIndicator,
  _gpsIndicator,
  _batStatusIndicator
};

fileTipo _myFileTipo;
urlTipo _myUrlTipo;
tipoTag _tipoTag;
struct Jaco_parametros *myJaco = newJacoParametros();
struct File_names *jacoFilenames = newFileNames();
Tools myTools;
ModuloWdt wdt(WDT_DEFAULT);
SoundAlarm alarmaPersona(TONO_PERSONA, TONO_PERSONA_DELAY, (sizeof(TONO_PERSONA)) / (sizeof(TONO_PERSONA[0])), 2, PWR_AUDIO, SPEAKER_PIN);
SoundAlarm alarmaLabor(TONO_TAG, TONO_TAG_DELAY, (sizeof(TONO_TAG)) / (sizeof(TONO_TAG[0])), 1, PWR_AUDIO, SPEAKER_PIN);
SoundAlarm alarmaGps(TONO_ALARM_GPS, TONO_ALARM_GPS_DELAY, (sizeof(TONO_ALARM_GPS)) / (sizeof(TONO_ALARM_GPS[0])), 2, PWR_AUDIO, SPEAKER_PIN);
SoundAlarm alarmaSync(TONO_TAG, TONO_TAG_DELAY, (sizeof(TONO_TAG)) / (sizeof(TONO_TAG[0])), 2, PWR_AUDIO, SPEAKER_PIN);
SoundAlarm alarmaMemoria(TONO_PERSONA, TONO_PERSONA_DELAY, (sizeof(TONO_PERSONA)) / (sizeof(TONO_PERSONA[0])), 1, PWR_AUDIO, SPEAKER_PIN);
LedRGB configModeIndicator("Azul", 0, 0, PINES_RGB, N_PINES);
LedRGB gpsIndicator(TIEMPO_ON, 1, PINES_RGB, N_PINES);
LedRGB batStatusIndicator(0, 0, PINES_RGB, N_PINES);
LedRGB syncIndicator(100, 2, PINES_RGB, N_PINES);
LedRGB memIndicator("Blanco", 0, 0, PINES_RGB, N_PINES);

SFE_MAX1704X nLipo(MAX1704X_MAX17048);
BatStatus lipo(BAT_PIN, nLipo);
ModuloSD storage(CS_PIN_SD, jacoFilenames, myJaco, _myFileTipo, memIndicator, wdt, _DEBUG_MODE_);
ModuloWiFi wifi(PWR_WIFI, wifiSerial, storage, _myUrlTipo, lipo, syncIndicator, wdt, _DEBUG_MODE_);
ModuloGPS gps(PWR_GPS, gpsSerial, _DEBUG_MODE_);
ModuloRFID rfid(PWR_5V, Serial1, _tipoTag, _DEBUG_MODE_);
FlashStorage(memoryDebug, bool);
RTC_SAMD51 rtc;

void setup()
{
  myJaco->SetLenTramaCoordenada(myJaco, TAMANO_TRAMA_COORDENADAS);
  myJaco->SetLenTramaOperaciones(myJaco, TAMANO_TRAMA_OPERACIONES);
  myJaco->SetLenTrama2Labors(myJaco, TAMANO_TRAMA_2LABORS);
  myJaco->SetLenTramaParametros(myJaco, TAMANO_TRAMA_PARAMETROS);
  myJaco->SetServerParameters(myJaco, SERVER_JACO_PARAMETERS);
  myJaco->SetHeaders(myJaco, HEADERS_TRAMAS);
  myJaco->SetLenData(myJaco, LEN_DATA);
  myJaco->SetFormatData(myJaco, FORMAT_DATA);  
  jacoFilenames->SetFileNames(jacoFilenames, FILE_NAMES_JACO);
  //  //Inicializacion de puertos Seriales y SPI
  pinPeripheral(50, PIO_SERCOM_ALT);
  pinPeripheral(51, PIO_SERCOM);
  pinPeripheral(11, PIO_SERCOM);
  pinPeripheral(13, PIO_SERCOM);
  Serial.begin(9600);
  // while(!Serial);
  gps.Begin(GPS_BAUDRATE);
  rfid.Begin(RFID_BAUDRATE);
  storage.Begin();
  wifi.Begin(WIFI_BAURATE);
  Wire.begin();
  rtc.begin();
  lipo.Init();
#ifdef _JACO_V3_
  pinMode(IRQ_PIN_BOTON, INPUT_PULLUP);
#endif

  // Inicializacion pines
  pinMode(IRQ_PIN_RFID, INPUT_PULLUP);

  delay(50);
  Stop_wifi();

  debugEnable = memoryDebug.read();

  myJaco->GetJacoID(myJaco) != 0 ? zanteIdExists = true : zanteIdExists = false;
  HEADERS[1] = String(myJaco->GetJacoID(myJaco));

  zanteIdExists ? Activar_indicador(_batStatusIndicator, lipo.Get_color_feedback()) : Activar_indicador(_batStatusIndicator, "Blanco");

  delay(5);

  if (debugEnable)
  {
    memoryDebug.write(false); // el print esta desabilitado por defecto,
  }

  wdt.Enable(); // Se programa wdt con 5 segundos para reinicio por fallos de sistema
}
void loop()
{
  wdt.Reset();
  Handle_feedbacks();
  voltajeBat = lipo.Get_voltaje();
  if (JACO_DESCONECTADO)
  {
    cont += 1;

    if (!gpsActivado)
    {
      Init_gps();
    }

    if (!ledStatus)
    {
      timeOn = millis();
    }
    firstCharge = true; // Se pone en true la bandera de primera carga

    if (voltajeBat < 3.3)
    {
      Stop_rfid();
#ifdef _JACO_V3_
      detachInterrupt(digitalPinToInterrupt(IRQ_PIN_BOTON));
#endif
    }

    // Cada minuto almaceno los datos de parametros
    if (Periodo_muestreo(temporizadorZanteps, TIEMPO_ZANTEPS) && dataTimeFixed)
    {
      if (datosGps[2] == "")
      {
        myTools.String_fill_beg(&datosGps[2], '0', 19); // relleno de ceros el valor hasta tener 19 caracteres
      }

      float temperatura = Get_tempc();
      String cTemp = String(temperatura);
      myTools.String_fill_beg(&cTemp, '0', 6); ////relleno de ceros el valor hasta tener 6 caracteres
      String datosParametros[4];
      datosParametros[0] = String(voltajeBat);
      datosParametros[1] = "0";
      datosParametros[2] = datosGps[2];
      datosParametros[3] = cTemp;
      _myFileTipo = _fParametros;
      // Set_file_tipo(_myFileTipo);
      Guardar_datos(datosParametros, sizeof(datosParametros) / sizeof(datosParametros[0]));
    }

    if (Fixed_gps())
    {
      Show("Vector de coordenadas lleno...\n");
      //----------------------------------------------------------------------------------------------------------------------------------------
      //--------------------------------ACTIVACION INTERRUPCION RFID----------------------------------------------------------------------------
      //----------------------------------------------------------------------------------------------------------------------------------------
      if (!rfidActivado && voltajeBat > 3.3)
      {
        Init_rfid();
#ifdef _JACO_V3_
        attachInterrupt(digitalPinToInterrupt(IRQ_PIN_BOTON), Boton_int, FALLING);
#endif
      }

      //----------------------------------------------------------------------------------------------------------------------------------------
      //------------------------------------INTERRUPCION ALMACENAMIENTO OPERACIONES----------------------------------------------------------------------------
      //----------------------------------------------------------------------------------------------------------------------------------------
#ifdef _JACO_V3_
      if (bNewInt)
      {
        bNewInt = false;
        if (tagLeidoMem != "")
        {
          tagLeido[0] = tagLeidoMem;
          Save_operation(datosGps, tamanoDatosGps - 1);
        }
      }
#endif

      if (aNewInt)
      {
        aNewInt = false;

        Receive_rfid(); // Leemos los datos del tag

        if (Periodo_muestreo(temporizadorInterrupcion, TIEMPO_INTERRUPCION) || !myTools.Compare_vect_string(tagLeidoAnterior, tagLeido, sizeof(tagLeido) / sizeof(tagLeido[0])))
        {
          primeraVez = true;
        }
        if (primeraVez)
        {
          // Si el dato leido es valido guardamos la operacion en la memoria
          if (tagLeido[0] != "" && tagLeido[0] != "0")
          {
            Set_gps_filter(false);
            if (_tipoTag == _labor || _tipoTag == _2labors)
            {
              Save_operation(datosGps, tamanoDatosGps - 1);
            }
            else if (_tipoTag == _persona)
            {
              Set_personaID(tagLeido[0].toInt());
              Activar_alarma(_alarmaPersona);
            }
            else if (_tipoTag == _memoria)
            {
              tagLeidoMem = tagLeido[0];
              Activar_alarma(_alarmaMemoria);
            }
          }
          else
          {
            if (aNewInt && tagLeido[0] == "")
              Show("@@@@@@@@@@@@@@@ ERROR LEYENDO TAG @@@@@@@@@@@@@@@@@@@@@\n");

            tagLeido[0] = "";
            aNewInt = false;
          }
          primeraVez = false;
        }
        else
        {
          Show("@@@@@@@@@@@@@@@ ERROR - Marcacion doble @@@@@@@@@@@@@@@\n");
          delay(10);
        }
      }
      else
      {
        tagLeido[0] = "";
      }
    }
    //----------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------VALIDACION GPS------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------
    bool gpsValido = Get_datos_gps(datosGps); // Tomamos los datos del gps y verificamos si son validos
    // Si los datos de gps son validos
    if (gpsValido)
    {
      cont = 0;
      temporizadorAlarmaGps = millis();

      if (!lostGpsAlarm)
      {
        lostGpsAlarm = true;
        GpsAlarm = false;
        Desactivar_alarmas();
      }

      if (!dataTimeFixed)
      {
        Adjust_date_time(datosGps[2]);
        dataTimeFixed = true;
      }
      zanteIdExists ? Activar_indicador(_gpsIndicator, lipo.Get_color_feedback(), TIEMPO_OFF_5S) : Activar_indicador(_gpsIndicator, "Blanco", TIEMPO_OFF_5S);

      //----------------------------------------------------------------------------------------------------------------------------------------
      //------------------------------------ALMACENAMIENTO COORDENADAS-----------------------------------------------------------------------
      //----------------------------------------------------------------------------------------------------------------------------------------
      // Si los vectores de coordenadas estan llenos y se cumple el periodo de muestreo guardo los datos en la memoria
      if (Fixed_gps())
      {
        if (Periodo_muestreo(temporizadorMuestreoGps, TIEMPO_MUESTREO_GPS) && Filtered_gps())
        {
          Show("entre a guardar datos**************************\n");
          Show("Datos a guardar: ");
          Show(datosGps[0] + "\n");
          Show(datosGps[1] + "\n");
          _myFileTipo = _fCoordenadas;
          Set_file_tipo(_myFileTipo);
          Guardar_datos(datosGps, tamanoDatosGps); // guardo los datos en la memoria - tabla coordenadas
        }

        //----------------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------ACTIVACION INTERRUPCION RFID----------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------------------------------------------------
        if (!rfidActivado && voltajeBat > 3.3)
        {
          Init_rfid();
#ifdef _JACO_V3_
          attachInterrupt(digitalPinToInterrupt(IRQ_PIN_BOTON), Boton_int, FALLING);
#endif
        }

        //----------------------------------------------------------------------------------------------------------------------------------------
        //------------------------------------INTERRUPCION ALMACENAMIENTO OPERACIONES----------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------------------------------------------------
#ifdef _JACO_V3_
        if (bNewInt)
        {
          bNewInt = false;
          if (tagLeidoMem != "")
          {
            tagLeido[0] = tagLeidoMem;
            Save_operation(datosGps, tamanoDatosGps - 1);
          }
        }
#endif
        if (aNewInt)
        {
          aNewInt = false;

          Receive_rfid(); // Leemos los datos del tag

          if (Periodo_muestreo(temporizadorInterrupcion, TIEMPO_INTERRUPCION) || !myTools.Compare_vect_string(tagLeidoAnterior, tagLeido, sizeof(tagLeido) / sizeof(tagLeido[0])))
          {
            primeraVez = true;
          }
          if (primeraVez)
          {
            // Si el dato leido es valido guardamos la operacion en la memoria
            if (tagLeido[0] != "" && tagLeido[0] != "0")
            {
              Set_gps_filter(false);
              if (_tipoTag == _labor || _tipoTag == _2labors)
              {
                Save_operation(datosGps, tamanoDatosGps - 1);
              }
              else if (_tipoTag == _persona)
              {
                Set_personaID(tagLeido[0].toInt());
                Activar_alarma(_alarmaPersona);
              }
              else if (_tipoTag == _memoria)
              {
                tagLeidoMem = tagLeido[0];
                Activar_alarma(_alarmaMemoria);
              }
            }
            else
            {
              if (aNewInt && tagLeido[0] == "")
                Show("@@@@@@@@@@@@@@@ ERROR LEYENDO TAG @@@@@@@@@@@@@@@@@@@@@\n");

              tagLeido[0] = "";
              aNewInt = false;
            }
            primeraVez = false;
          }
          else
          {
            Show("@@@@@@@@@@@@@@@ MARCACION DOBLE @@@@@@@@@@@@@@@@@@@@@\n");
            delay(10);
          }
        }
        else
        {
          tagLeido[0] = "";
        }
      }
    }

    else
    {
      Show("Coordenadas no validas\n");

      if (cont > 400)
      {
        if (lostGpsAlarm)
        {
          if (Periodo_muestreo(temporizadorAlarmaGps, TIEMPO_ALARMA_GPS))
          {
            GpsAlarm = true;
            lostGpsAlarm = false;
          }
        }
        if (dataTimeFixed)
        {
          datosGps[2] = Get_date_time();
        }
        // Enciendo led cada 1 segundos
        zanteIdExists ? Activar_indicador(_gpsIndicator, lipo.Get_color_feedback(), TIEMPO_OFF_1S) : Activar_indicador(_gpsIndicator, "Blanco", TIEMPO_OFF_1S);
        if (GpsAlarm)
        {
          Activar_alarma(_alarmaGps, TRIGGER_TIME_ALARMA_GPS);
        }
      }
    }
  }
  else
  {
    //----------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------CARGADOR CONECTADO---------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------

    if (firstCharge)
    {
      lostGpsAlarm = false;
      GpsAlarm = false;
      dataTimeFixed = false;
      firstCharge = false;
      Desactivar_alarmas();

      Show("##################CARGADOR CONECTADO###############\n");
      Show("RFID Apagado...\n");
      Show("GPS Apagado...\n");
      Show("Modulo WiFi Encendiendo...\n");
#ifdef _JACO_V3_
      detachInterrupt(digitalPinToInterrupt(IRQ_PIN_BOTON));
#endif
      Stop_rfid();
      Stop_gps(); // apago gps

      if (!wifiActivado)
      {
        Init_wifi();
      }
      /*Guardo cierre de dia (Por que termine labor)*/
      Save_cierre_Dia(datosGps[2], myJaco->GetPersonaID(myJaco));

      /*Funcion para configurar JACO por consola*/
      jacoConfigured = Config_jaco();
    }

    if (jacoConfigured)
    {
      // Si hay datos pendientes por subir Sincronizamos
      if (Pendiente_por_subir())
      {
        if (Connect_to_wifi())
        { // Nos conectamos el wifi
          if (zanteIdExists)
          {
            Sync_field_data();
          }
          else
          {
            firstCharge = true;
          }
        }
        if (!Pendiente_por_subir())
        {
          Activar_alarma(_alarmaSync);
        }
      } // Si no hay datos por sincronizar
      else
      {
        Show("No hay datos por sincronizar\n");
        Show("Fin...\n");

        Charging_mode(); // Espera  15 min mostrando nivel de bateria
      }
    }
    else
    {
      Charging_mode(120000); // Espera de 2 min mostrando el nivel de bateria
    }
  }
}

void Init_gps()
{
  gps.PwrON();
  gpsActivado = gps.Status();
}

void Stop_gps()
{
  gps.PwrOFF();
  gpsActivado = gps.Status();
}

bool Fixed_gps()
{
  return gps.Fixed();
}

bool Get_datos_gps(String tablaGps[])
{
  return gps.Get_data(tablaGps);
}

void Set_gps_filter(bool value)
{
  gps.Set_filter(value);
}

bool Filtered_gps()
{
  return gps.Filter_passed();
}

void Show(byte value)
{
  myTools.Print(value, _DEBUG_MODE_);
}

void Show(int value)
{
  myTools.Print(value, _DEBUG_MODE_);
}

void Show(float value)
{
  myTools.Print(value, _DEBUG_MODE_);
}
void Show(double value)
{
  myTools.Print(value, _DEBUG_MODE_);
}

void Show(String value)
{
  myTools.Print(value, _DEBUG_MODE_);
}

void Show(char value)
{
  myTools.Print(value, _DEBUG_MODE_);
}
////----------------------------------------------------------------------------------------------------------------------------------------
////-------------------------------BANDERA INTERRUPCION RFID-------------------------------------------------------------------------------------
////----------------------------------------------------------------------------------------------------------------------------------------
void Rfid_int()
{
  if (millis() - debounceStartTime > DEBOUNCE_TIME_THRESHOLD)
  {
    aNewInt = true;
    debounceStartTime = millis();
  }
}

void Boton_int()
{
  if (millis() - debounceStartTime_button > DEBOUNCE_TIME_THRESHOLD)
  {
    bNewInt = true;
    debounceStartTime_button = millis();
  }
}
////----------------------------------------------------------------------------------------------------------------------------------------
////---------------------------------FUNCION PARA VALIDAR PERIODO MUESTREO DEL GPS----------------------------------------------------------------------
////----------------------------------------------------------------------------------------------------------------------------------------
bool Periodo_muestreo(unsigned long &tiempo_anterior, unsigned long muestreo)
{
  bool periodo = false;
  unsigned long tiempo_actual = millis();
  if (tiempo_anterior == 0)
  {
    tiempo_anterior = tiempo_actual;
  }
  if ((unsigned long)(tiempo_actual - tiempo_anterior) >= muestreo)
  {
    periodo = true;
    tiempo_anterior = tiempo_actual;
  }
  else
  {
    periodo = false;
  }
  return periodo;
}

////----------------------------------------------------------------------------------------------------------------------------------------
////-------------------------------FUNCION PARA SUBIR PAQUETE CREADO EN SINCRONIZAR---------------------------------------------------------
////----------------------------------------------------------------------------------------------------------------------------------------

String Request_to_server(urlTipo url, String payload = "")
{
  String tipo = "";
  // String _myUrlTipo = myJaco->GetUrl(myJaco, url);
  String fingerprint = myJaco->GetFingerPrint(myJaco);

  if (url == _operaciones || url == _coordenadas || url == _parametros || url == _cierreDia || url == _update || url == _mac)
  {
    tipo = "POST";
  }
  else if (url == _date || url == _wifis || url == _fingerprint)
  {
    tipo = "GET";
  }
  else
  {
    tipo = "GET";
  }
  wifi.Empty_buffer();
  delay(100);
  return wifi.Request_to_server(myJaco->GetUrl(myJaco, url), payload, tipo, myJaco->GetFingerPrint(myJaco), HEADERS, SIZE_HEADERS, myJaco->GetToken(myJaco), myJaco->GetServer(myJaco));
}
//----------------------------------------------------------------------------------------------------------------------------------------
//------------------------------FUNCION PARA CREAR PAQUETE QUE SERA SINCRONIZADO----------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------
void Sync_field_data()
{
  wdt.Disable();
  myTools.Println("sincronizar operaciones", debugEnable);
  _myUrlTipo = _operaciones;
  wifi.Sync_protocol(HEADERS, SIZE_HEADERS);

  myTools.Println("sincronizar 2labors", debugEnable);
  _myUrlTipo = _2Labors;
  wifi.Sync_protocol(HEADERS, SIZE_HEADERS);

  myTools.Println("sincronizar coordenadas", debugEnable);
  _myUrlTipo = _coordenadas;
  wifi.Sync_protocol(HEADERS, SIZE_HEADERS);

  _myUrlTipo = _cierreDia;
  myTools.Println("sincronizar cierre de dia", debugEnable);
  Sync_cierre_dia ();

  myTools.Println("sincronizar parametros", debugEnable);
  _myUrlTipo = _parametros;
  wifi.Sync_protocol(HEADERS, SIZE_HEADERS);
  wdt.Enable();
}

bool Pendiente_por_subir()
{
  bool existsOperFile = storage.Check_file_existence(jacoFilenames->GetFileOperaName(jacoFilenames));
  bool existsCoorFile = storage.Check_file_existence(jacoFilenames->GetFileCoordName(jacoFilenames));
  bool existsParaFile = storage.Check_file_existence(jacoFilenames->GetFileParamName(jacoFilenames));
  if (existsOperFile || existsCoorFile || existsParaFile)
  {
    return true;
  }
  else
  {
    return false;
  }
}

void Stop_rfid()
{
  rfid.PwrOFF();
  rfidActivado = rfid.Status();
  detachInterrupt(digitalPinToInterrupt(IRQ_PIN_RFID));
}

void Init_rfid()
{
  rfid.PwrON();
  rfidActivado = rfid.Status();
  attachInterrupt(digitalPinToInterrupt(IRQ_PIN_RFID), Rfid_int, FALLING);
}

void Receive_rfid()
{
  rfid.Receive();
  rfid.Get_data(tagLeido);
  // rfid.Get_tipo();
}

void Save_operation(String datos[], unsigned char tamanoDatos)
{
  if (datos[2] != "") // La fecha no debe estar vacia
  {
    int nLaborParms = 1;
    if (tagLeido[1] != "")
    {
      nLaborParms++;
      _myFileTipo = _f2Labors;
    }
    else
    {
      _myFileTipo = _fOperaciones;
    }
    String datoLaborToSave[tamanoDatos + nLaborParms]; // creo vector para almacenar los datos que seran grabados en
    myTools.Match_vect_string(tagLeido, tagLeidoAnterior, sizeof(tagLeido) / sizeof(tagLeido[0]));

    for (int i = 0; i < tamanoDatos; i++) // tabla operaciones
    {
      datoLaborToSave[i] = datos[i];
    }

    for (int i = 0; i < nLaborParms; i++)
    {
      myTools.String_fill_beg(&tagLeido[i], '0', 10); // relleno de ceros el valor hasta tener 10 caracteres
      datoLaborToSave[tamanoDatos + i] = tagLeido[i]; // En la ultima posicion del vector almaceno la labor id leida
      tagLeido[i] = "";
    }

    bool guardado = storage.Guardar_datos(datoLaborToSave, sizeof(datoLaborToSave) / sizeof(datoLaborToSave[0])); //  guardo datos en tabla operaciones

    if (guardado)
    {
      Activar_alarma(_alarmaLabor);
    }
  }
  aNewInt = false; // bajo bandera rfid
}

bool Config_jaco()
{
  if (JACO_DESCONECTADO)
  {
    Activar_indicador(_configModeIndicator, "Apagado");
    wdt.Reset_device(100);
  }
  else
  {
    wdt.Disable();
    zanteIdExists ? Activar_indicador(_configModeIndicator) : Activar_indicador(_configModeIndicator, "Blanco");

    unsigned long timeOutUSb = millis();
    while (!Serial && millis() - timeOutUSb < 10000) // Espero 10 segundos a que se conecten por USB
      ;
    unsigned long tiempoFinConfig = TIEMPO_FIN;
    if (!Serial)
    {
      tiempoFinConfig = 5000;
    }
    char input = '*';
    char sInput = '0';
    char tInput = '0';
    int estado = 0;
    int estadoSoporte = 0;
    int estadoSd = 0;
    String cadena = "";
    bool finConfig = false;
    bool estadoSSL = false;
    String secrets[2];
    bool redesCompletas = false;
    bool vectorBool[3];
    bool showScreen = false; // No se muestra menu serial hasta que se ingrese el caracter '#'
    char atCommand = 0x00;
    bool wifiConnected = false;
    bool stop = false;
    _myFileTipo = _fPersonaID;
    storage.Delete();
    tiempoActual = millis();
    while (millis() - tiempoActual < tiempoFinConfig && !finConfig)
    {
      if (JACO_DESCONECTADO)
      {
        Activar_indicador(_configModeIndicator, "Apagado");
        wdt.Reset_device(100);
      }
      // for (int i = 0; i < 3; i++)
      //{
      switch (estado)
      {
        case 0:

          if (showScreen)
          {
            Print_msg_1();
            showScreen = false;
            tiempoActual = millis();
          }
          if (input == '1')
          {
            estado = 1;
            showScreen = true;
          }
          else if (input == '2')
          {
            estado = 2;
            showScreen = true;
          }
          else if (input == '3')
          {
            estado = 3;
            showScreen = true;
          }
          else if (input == '4')
          {
            estado = 4;
            showScreen = true;
          }
          else if (input == '0')
          {
            finConfig = true;
          }
          else if (input == '*')
          {
            estado = 0;
          }
          else if (input == '#')
          {
            estado = 0;
            showScreen = true;
          }
          break;

        case 1:
          if (showScreen)
          {
            Print_redes(NAME_SECRET_FILE, sizeof(NAME_SECRET_FILE) / sizeof(NAME_SECRET_FILE[0]));
            Serial.println("(0) Atras");
            showScreen = false;
            tiempoActual = millis();
          }
          if (input == '0')
          {
            estado = 0;
            showScreen = true;
          }
          break;
        case 2:
          if (showScreen)
          {
            Serial.print("Ingresar el nombre de la nueva red: ");
            secrets[0] = "";
            tiempoActual = millis();
            tiempoActual2 = millis();
            bool finInput = false;
            while (millis() - tiempoActual2 < TIEMPO_FIN_2 && !finInput)
            {
              while (Serial.available() > 0)
              {
                char c = Serial.read();
                if (c != '\n')
                {
                  secrets[0] += c;
                }
                if (secrets[0] != "")
                {
                  finInput = true;
                }
              }
            }
            secrets[0] = secrets[0].substring(0, secrets[0].length() - 1); // quitamos caracter fantasma
            Vaciar_buffer_serial();
            Serial.println(secrets[0]);
            if (secrets[0] != "")
            {
              finInput = false;
              Serial.print("Ingresar contraseña: ");
              secrets[1] = "";
              tiempoActual = millis();
              tiempoActual2 = millis();
              while (millis() - tiempoActual2 < TIEMPO_FIN_2 && !finInput)
              {
                while (Serial.available() > 0)
                {
                  char c = Serial.read();
                  if (c != '\n')
                  {
                    secrets[1] += c;
                  }
                  else
                  {
                    finInput = true;
                  }
                }
              }
              if (secrets[1] != "")
              {
                secrets[1] = secrets[1].substring(0, secrets[1].length() - 1); // quitamos caracter fantasma
              }

              Vaciar_buffer_serial();
              for (int i = 0; i < secrets[1].length(); i++)
              {
                Serial.print("*");
              }
              Serial.println();
              tiempoActual = millis();

              Guardar_red(secrets, NAME_SECRET_FILE[1]);

              Serial.println("Validando...");
              if (Connect_to_wifi())
              {
                Serial.println("WiFi Conectado\r\n");
                Serial.println("Conectado");
                Serial.println("Por favor elija una opción: ");
                Serial.println("(1) Sincronizar");
                Serial.println("(0) Atras");
                estado = 2;
                input = '4';
              }
              else
              {
                Serial.println("No fue posible conectarse a la red");
                Serial.println("Red guardada correctamente\r\n");
                Serial.println("Por favor elija una opción: ");
                Serial.println("(1) Sincronizar");
                Serial.println("(0) Atras");
                estado = 2;
                input = '4';
              }
            }
            tiempoActual = millis();
            showScreen = false;
          }
          if (input == '1')
          {
            finConfig = true;
          }
          else if (input == '0')
          {
            estado = 0;
            showScreen = true;
          }
          else if (input == '2')
          {
            estado = 2;
            showScreen = true;
          }
          break;
        case 3:

          sInput = input;
          switch (estadoSoporte)
          {
            case 0:
              if (showScreen)
              {
                Print_msg_3();
                showScreen = false;
                tiempoActual = millis();
              }
              if (sInput == '1')
              {
                input = 3;
                estadoSoporte = 1;
                showScreen = true;
              }
              else if (sInput == '2')
              {
                input = 3;
                estadoSoporte = 2;
                showScreen = true;
              }
              else if (sInput == '3')
              {
                input = 3;
                estadoSoporte = 3;
                showScreen = true;
              }
              else if (sInput == '4')
              {
                input = 3;
                estadoSoporte = 4;
                showScreen = true;
              }
              else if (sInput == '5')
              {
                input = 3;
                estadoSoporte = 5;
                showScreen = true;
              }
              else if (sInput == '6')
              {
                input = 3;
                estadoSoporte = 6;
                showScreen = true;
              }
              else if (sInput == '7')
              {
                input = 3;
                estadoSoporte = 7;
                showScreen = true;
              }
              else if (sInput == '8')
              {
                input = 3;
                estadoSoporte = 8;
                showScreen = true;
              }
              else if (sInput == '0')
              {
                input = 3;
                estadoSoporte = 9;
                showScreen = true;
              }
              break;

            case 1:
              if (showScreen)
              {
                debugEnable = memoryDebug.read();
                if (debugEnable)
                {
                  Set_modules_debugger(true);
                  myTools.Println("Modo debug activado", debugEnable);
                  myTools.Println("(0) Atras", debugEnable);
                  showScreen = false;
                }
                else
                {
                  memoryDebug.write(true);
                  delay(4);
                  showScreen = true;
                }
              }
              if (sInput == '0')
              {
                input = 3;
                showScreen = true;
                estadoSoporte = 0;
              }
              break;

            case 2:
              tInput = sInput;
              if (showScreen)
              {
                storage.Read_write_document("test.txt", 'w', true, "test de memoria..."); // Creo un archivo de nombre test.txt
                storage.Read_write_document("test.txt", 'r', true, "");                   // Leo el archivo creado
              }

              switch (estadoSd)
              {
                case 0:
                  if (showScreen)
                  {
                    Print_msg_4();
                    showScreen = false;
                    tiempoActual = millis();
                  }

                  if (tInput == '1')
                  {
                    sInput = 2;
                    estadoSd = 1;
                    showScreen = true;
                  }
                  else if (tInput == '2')
                  {
                    sInput = 2;
                    estadoSd = 2;
                    showScreen = true;
                  }
                  else if (tInput == '3')
                  {
                    sInput = 2;
                    estadoSd = 3;
                    showScreen = true;
                  }
                  else if (tInput == '0')
                  {
                    sInput = 2;
                    estadoSd = 4;
                    showScreen = true;
                  }

                  break;
                case 1:
                  if (showScreen)
                  {
                    storage.Read_write_document(jacoFilenames->GetFileOperaName(jacoFilenames), 'r', true, "");
                    Serial.println("(0) Atras");
                    showScreen = false;
                    tiempoActual = millis();
                  }

                  if (tInput == '0')
                  {
                    sInput = 2;
                    estadoSd = 0;
                    showScreen = true;
                  }
                  break;
                case 2:
                  if (showScreen)
                  {
                    storage.Read_write_document(jacoFilenames->GetFileCoordName(jacoFilenames), 'r', true, "");
                    Serial.println("(0) Atras");
                    showScreen = false;
                    tiempoActual = millis();
                  }

                  if (tInput == '0')
                  {
                    sInput = 2;
                    estadoSd = 0;
                    showScreen = true;
                  }
                  break;
                case 3:
                  if (showScreen)
                  {
                    storage.Read_write_document(jacoFilenames->GetFileParamName(jacoFilenames), 'r', true, "");
                    Serial.println();
                    Serial.println("(0) Atras");
                    showScreen = false;
                    tiempoActual = millis();
                  }

                  if (tInput == '0')
                  {
                    sInput = 2;
                    estadoSd = 0;
                    showScreen = true;
                  }
                  break;
                case 4:
                  input = '0';
                  if (input == '0')
                  {
                    estado = 3;
                    estadoSoporte = 0;
                    estadoSd = 0;
                    showScreen = true;
                  }
                  break;
              }

              if (sInput == '4')
              {
                input = 3;
                showScreen = true;
                estadoSoporte = 0;
              }
              break;

            case 3:
              Activar_alarma(_alarmaLabor);
              delay(500);
              if (showScreen)
              {
                Serial.println("Test parlante...");
                Serial.println("Si no escucha nada revisar el parlante...");
                Serial.println("(0) Atras");
                showScreen = false;
              }
              if (sInput == '0')
              {
                input = 3;
                showScreen = true;
                estadoSoporte = 0;
              }
              break;

            case 4:
              if (showScreen)
              {
                Serial.println("Test RFID: Por favor Pasar tarjeta...");
                Serial.println("(0) Atras");
                showScreen = false;
              }
              if (!rfidActivado)
              {
                Init_rfid();
              }

              if (aNewInt)
              {
                aNewInt = false;
                Receive_rfid(); // Leemos los datos del tag
              }
              if (tagLeido[0] != "")
              {
                Serial.println("------------------------");
                Serial.print("Tipo tag: ");
                if (_tipoTag == _labor || _tipoTag == _2labors)
                {
                  Serial.println("Tag de labor");
                  Serial.print("Tag leido: ");
                  Serial.println(tagLeido[0] + " " + tagLeido[1]);
                  Activar_alarma(_alarmaLabor);
                }
                else if (_tipoTag == _persona)
                {
                  Serial.println("Tag de persona");
                  Serial.print("Tag leido: ");
                  Serial.println(tagLeido[0] + " " + tagLeido[1]);
                  Set_personaID(tagLeido[0].toInt());
                  Activar_alarma(_alarmaPersona);
                }
                else if (_tipoTag == _memoria)
                {
                  Serial.println("Tag de memoria");
                  Serial.print("Tag leido: ");
                  Serial.println(tagLeido[0] + " " + tagLeido[1]);
                  tagLeidoMem = tagLeido[0];
                  Activar_alarma(_alarmaMemoria);
                }
                Serial.println("\nTest RFID exitoso...");
                Serial.println("(0) Atras");
                Serial.println("------------------------");
                tagLeido[0] = "";
              }
              if (sInput == '0')
              {
                input = 3;
                showScreen = true;
                estadoSoporte = 0;
                Stop_rfid();
              }
              break;

            case 5:
              if (showScreen)
              {
                wifi.Empty_buffer();
                Serial.println(wifi.Test());

                Serial.println("Test WiFi: ");
                Serial.println("(0) Atras");
              }
              showScreen = false;
              if (sInput == '0')
              {
                sInput = 3;
                showScreen = true;
                estadoSoporte = 0;
              }
              break;

            case 6:
              {
                String secrets = "";
                bool finInput = false;
                Serial.print("Ingresar contraseña: ");
                secrets = "";
                tiempoActual = millis();
                tiempoActual2 = millis();
                while (millis() - tiempoActual2 < TIEMPO_FIN_2 && !finInput)
                {
                  while (Serial.available() > 0)
                  {
                    char c = Serial.read();
                    if (c != '\n')
                    {
                      secrets += c;
                    }
                    if (secrets != "")
                    {
                      finInput = true;
                    }
                  }
                }
                secrets = secrets.substring(0, secrets.length() - 1); // quitamos caracter fantasma
                Vaciar_buffer_serial();
                for (int i = 0; i < secrets.length(); i++)
                {
                  Serial.print("*");
                }
                Serial.println();

                if (secrets == USERMASTER_KEY)
                {
                  storage.Restore();
                  Serial.println("\nEquipo restaurado");
                  Serial.println("\nA continuacion debera configurar la red WiFi\n");
                  sInput = '7';
                }
                else
                {
                  Serial.println("Contraseña incorrecta");
                  Serial.println("Intentar nuevamente\r\n");
                  sInput = '3';
                }
              }
              if (sInput == '3')
              {
                // input = '3';
                // sInput = '6';
                showScreen = true;
              }
              else if (sInput == '7')
              {
                // input = '7';
                estadoSoporte = 9; // antes 8
                showScreen = true;
              }
              break;

            case 7:
              Sleep_now();
              break;

            case 8:
              {
                if (showScreen)
                {
                  tiempoActual = millis();
                  bool finInput = false;
                  wifi.Empty_buffer();
                  String sslState = wifi.Check_ssl();

                  if (sslState == "SSL_ON")
                  {
                    estadoSSL = true;
                    Serial.println();
                    Serial.println("SSL Encendido...");
                    Serial.println("(1) Apagar");
                    Serial.println("(0) Atras");
                  }
                  else if (sslState == "SSL_OFF")
                  {
                    estadoSSL = false;
                    Serial.println();
                    Serial.println("SSL Apagado...");
                    Serial.println("(1) Encender");
                    Serial.println("(0) Atras");
                  }
                  else
                  {
                    Serial.println("\nHardware no compatible...");
                    Serial.println("(0) Atras");
                  }
                  showScreen = false;
                }

                if (sInput == '1')
                {
                  if (estadoSSL)
                  {
                    wifi.Setup_ssl("OFF");
                  }
                  else
                  {
                    wifi.Setup_ssl("ON");
                  }

                  sInput = 3;
                  showScreen = true;
                  estadoSoporte = 8;
                }
                else if (sInput == '0')
                {
                  sInput = 3;
                  showScreen = true;
                  estadoSoporte = 0;
                }
              }
              break;

            case 9:
              input = '7';
              if (input == '7')
              {
                estado = 0;
                estadoSoporte = 0;
                showScreen = true;
              }
              break;
          }
          break;
        case 4:
          if (showScreen)
          {
            Serial.println("Ingrese el numero del JACO");
            bool finInput = false;
            String dataInput = "";
            tiempoActual = millis();
            tiempoActual2 = millis();
            while (millis() - tiempoActual2 < TIEMPO_FIN_2 && !finInput)
            {
              while (Serial.available() > 0)
              {
                char c = Serial.read();
                if (c != '\n')
                {
                  dataInput += c;
                }
                if (dataInput != "")
                {
                  finInput = true;
                }
              }
            }
            if (dataInput == "")
            {
              Serial.println("Tiempo de espera agotado");
            }
            else
            {
              if (dataInput.toInt() > 0)
              {
                if (Connect_to_wifi())
                {
                  tiempoActual = millis();
                  String payload = "";
                  String respuesta = "";
                  _myUrlTipo = _mac;
                  wifi.Empty_buffer();
                  delay(10);
                  String mac = wifi.Get_mac();

                  dataInput = dataInput.substring(0, dataInput.length() - 1);
                  if (mac.indexOf("MAC: ") >= 0)
                  {
                    mac = mac.substring(mac.indexOf("MAC: ") + 5, mac.indexOf("MAC: ") + 22); // Obtengo solo la mac
                    payload = "[{";
                    payload += "\"zante_id\":\"";
                    payload += dataInput;
                    payload += "\",\"mac\":\"";
                    payload += mac;
                    payload += "\"}]";
                    wdt.Disable();
                    respuesta = Request_to_server(_myUrlTipo, payload);
                    wdt.Enable();
                    if (respuesta.toInt() == 1)
                    {
                      tiempoActual = millis();
                      storage.Read_write_document(jacoFilenames->GetFileJacoIDName(jacoFilenames), 'w', false, String(dataInput.toInt()));
                      Serial.print("JACO_ID: ");
                      Serial.println(dataInput.toInt());
                      Serial.println("Asignado correctamente");
                      Serial.println("Por favor desconecte y reinicie el equipo");
                      mac = "";
                      payload = "";
                      respuesta = "";
                    }
                    else
                    {
                      Serial.println("ERROR: Fallo asignacion mac");
                      mac = "";
                      payload = "";
                      respuesta = "";
                    }
                  }
                  else
                  {
                    Serial.println("ERROR: Intente nuevamente");
                  }
                }
                else
                {
                  Serial.println("ERROR: Conectar red WiFi");
                }
              }
              else
              {
                Serial.println("ERROR: Intente nuevamente");
              }
            }
            Serial.println("(0) Atras");
          }
          showScreen = false;
          if (input == '0')
          {
            estado = 0;
            showScreen = true;
          }
          break;
      }
      if (Serial.available() > 0)
      {
        input = Serial.read();
        if (estadoSoporte == 5)
        {
          atCommand = input;
        }
        else
        {
          atCommand = 0x00;
        }
      }
      if (millis() - tiempoActual == tiempoFinConfig)
      {
        Serial.println("Tiempo de espera agotado");
      }
    }
    if (Connect_to_wifi())
    {
      wifiConnected = true;
      _myUrlTipo = _fingerprint;
      wdt.Disable();
      String fingerprintRecibido = Request_to_server(_myUrlTipo);
      wdt.Enable();

      if (fingerprintRecibido.indexOf("ERROR") < 0)
      {
        int index = fingerprintRecibido.indexOf('"');
        fingerprintRecibido.remove(0, index + 1);
        index = fingerprintRecibido.indexOf('"');
        fingerprintRecibido = fingerprintRecibido.substring(0, index);
      }
      else
      {
        fingerprintRecibido = "";
      }

      String fingerPrintAlmacenado = myJaco->GetFingerPrint(myJaco);

      if (fingerprintRecibido != fingerPrintAlmacenado && fingerprintRecibido != "")
      {
        storage.Set_fingerprint(fingerprintRecibido);
      }
      else if (fingerprintRecibido == "" && fingerPrintAlmacenado != "")
      {
        storage.Set_fingerprint(fingerPrintAlmacenado);
      }

      _myUrlTipo = _wifis;
      wdt.Disable();
      String wifis = Request_to_server(_myUrlTipo);
      wdt.Enable();

      if (wifis != "" && wifis.indexOf("ERROR") < 0)
      {
        int _index = wifis.indexOf('[');
        wifis.remove(0, _index);
        _index = wifis.indexOf(']');
        wifis = wifis.substring(0, _index + 1); // obtengo el json con las redes wifi

        Guardar_red(wifis, NAME_SECRET_FILE[0]);
      }
    }
    else
    {
      wifiConnected = false;
    }

    wdt.Enable();
    return wifiConnected;
  }
}

void Vaciar_buffer_serial()
{
  while (Serial.available())
  {
    Serial.read();
  }
}

void Guardar_red(String credentials[], String archivo)
{
  wdt.Reset();
  String trama = "";

  trama += "[{\"ssid\":\"";
  trama += credentials[0];
  trama += "\",\"psw\":\"";
  trama += credentials[1];
  trama += "\"}]";

  Guardar_red(trama, archivo);
}

void Guardar_red(String credentials, String archivo)
{
  wdt.Reset();
  storage.Read_write_document(archivo, 'w', false, credentials);
}

void Print_redes(String nameFile[], int numArchivos)
{
  String cadena[numArchivos];
  int j = 0;
  for (int i = 0; i < numArchivos; i++)
  {
    cadena[i] = storage.Read_write_document(nameFile[i], 'r', debugEnable, "");
    do
    {
      String ssid = myTools.Get_val_from_json(cadena[i], "ssid");
      if (ssid != "")
      {
        j++;
        Serial.print("(");
        Serial.print(j);
        Serial.print(")");
        Serial.println(ssid);
      }
      int index = cadena[i].indexOf('}');
      cadena[i].remove(0, index + 1);
      if (index == -1)
      {
        cadena[i] = "";
      }
    } while (cadena[i].length() > 0);
  }
}

void Print_msg_1()
{
  Serial.println('\n');
  Serial.println("----------------------------");
  Serial.print("JACO FIRMWARE V");
  Serial.println(CODE_VERSION);
  Serial.print("ZANTE_ID: ");
  Serial.println(myJaco->GetJacoID(myJaco));
  Serial.println("----------------------------");
  Serial.println("Por favor elija una opcion: ");
  Serial.println("(1) Mostrar redes guardadas");
  Serial.println("(2) Ingresar una nueva red");
  Serial.println("(3) Soporte");
  Serial.println("(4) Asignar JACO ID");
  Serial.println("(0) Salir");
  Serial.println("----------------------------");
}

void Print_msg_2()
{
  Serial.print("JACO FIRMWARE V");
  Serial.println(CODE_VERSION);
  Serial.print("ZANTE_ID:  ");
  Serial.println(myJaco->GetJacoID(myJaco));
  Serial.println("----------------------------");
  Serial.println("Por favor elija una opcion: ");
  Serial.println("(1) Mostrar redes guardadas");
  Serial.println("(2) Agregar una nueva red");
  Serial.println("(3) Soporte");
  Serial.println("(4) Mostrar direccion MAC");
  Serial.println("(0) Salir");
  Serial.println("----------------------------");
}

void Print_msg_3()
{
  Serial.println("Soporte...");
  Serial.println("----------------------------");
  Serial.println("Por favor elija una opcion: ");
  Serial.println("(1) Activar debug");
  Serial.println("(2) Test memoria SD");
  Serial.println("(3) Test parlante");
  Serial.println("(4) Test RFID");
  Serial.println("(5) Test WiFi");
  Serial.println("(6) Restaurar equipo");
  Serial.println("(7) Dormir");
  Serial.println("(8) Estado SSL");
  Serial.println("(0) Atras");
  Serial.println("----------------------------");
}

void Print_msg_4()
{
  Serial.println("----------------------------");
  Serial.println("Test memoria de SD finalizado...");
  Serial.println("Por favor elija una opcion: ");
  Serial.println("(1) Mostrar archivo de operaciones");
  Serial.println("(2) Mostrar archivo de coordenadas");
  Serial.println("(3) Mostrar archivo de parametros");
  Serial.println("(0) Atras");
  Serial.println("----------------------------");
}

void Charging_mode(unsigned long tempSync)
{
  if (JACO_DESCONECTADO)
  {
    Activar_indicador(_batStatusIndicator, "Apagado"); // Apago LED RGB
    wdt.Reset_device(100);
  }
  else
  {
    unsigned long timeSync = 0;
    bool stop = false;
    // unsigned long tempSync = 900000;

    digitalWrite(PWR_5V, LOW);   // apago 5V (rfid)
    digitalWrite(PWR_GPS, HIGH); // apago gps
    // digitalWrite(PWR_AUDIO, LOW); // Apago amplificador de audio
    detachInterrupt(digitalPinToInterrupt(IRQ_PIN_RFID));

    if (Connect_to_wifi())
    {
      _myUrlTipo = _update;
      String payload = "[{\"version\":"; //[{"version":2.0,"zante_id":1}]
      payload += CODE_VERSION;
      payload += ",\"zante_id\":";
      payload += String(myJaco->GetJacoID(myJaco));
      payload += "}]";
      wdt.Disable();
      String newVersion = Request_to_server(_myUrlTipo, payload);
      wdt.Enable();
      myTools.Println(newVersion, debugEnable);

      if (newVersion.length() > 1 && newVersion.indexOf("ERROR") < 0)
      {
        if (wifi.FTP_protocol(newVersion))
        {
          Serial.print("datos transeferidos...");
          wifi.Program_protocol(myTools.Get_val_from_json(newVersion, "path"));
        }
      }

      _myUrlTipo = _date;
      wdt.Disable();
      String dateTime = Request_to_server(_myUrlTipo);
      wdt.Enable();
      dateTime = myTools.Get_val_from_json(dateTime, "fecha");

      myTools.Println("Respuesta para sacar fecha y hora RECORTADO: ", debugEnable);
      myTools.Println(dateTime, debugEnable);

      if (dateTime != "" && dateTime.indexOf("ERROR") < 0)
      {
        Adjust_date_time(dateTime);
        dataTimeFixed = true;
      }

      Stop_wifi();
    }

    while (!Periodo_muestreo(timeSync, tempSync))
    {
      String conectado = "";
      delay(10);
      if (JACO_DESCONECTADO)
      {
        Activar_indicador(_batStatusIndicator, "Apagado"); // Apago LED RGB
        wdt.Reset_device(100);
      }
      else
      {
        zanteIdExists ? Activar_indicador(_batStatusIndicator, lipo.Get_color_feedback()) : Activar_indicador(_batStatusIndicator, "Blanco");

        conectado = "1";
      }
      if (Periodo_muestreo(timeSave, tempSave))
      {
        wdt.Reset();
        String fechaHora = "";
        if (dataTimeFixed)
        {
          fechaHora = Get_date_time();
        }
        else
        {
          myTools.String_fill_beg(&fechaHora, '0', 19); // relleno de ceros el valor hasta tener 19 caracteres
        }
        float temperatura = Get_tempc();
        String cTemp = String(temperatura);
        myTools.String_fill_beg(&cTemp, '0', 6); // relleno de ceros el valor hasta tener 6 caracteres
        String datosParametros[4];
        datosParametros[0] = String(voltajeBat); // voltaje de la bateria
        datosParametros[1] = conectado;          // esta conectado?
        datosParametros[2] = fechaHora;
        datosParametros[3] = cTemp;
        _myFileTipo = _fParametros;
        storage.Guardar_datos(datosParametros, sizeof(datosParametros) / sizeof(datosParametros[0]));
        wdt.Reset();
      }
      else
      {
        wdt.Reset(); // importante
      }
    }
    firstCharge = true;
  }
}

String Get_date_time()
{
  String dateTime = "";
  int ano, mes, dia, hora, minuto, segundo;
  String mesString, diaString, horaString, minutoString, segundoString = "";

  DateTime now = rtc.now();
  ano = now.year();
  mes = now.month();
  dia = now.day();
  hora = now.hour();
  minuto = now.minute();
  segundo = now.second();

  if (mes < 10)
  {
    mesString = "0" + String(mes);
  }
  else
  {
    mesString = String(mes);
  }

  if (dia < 10)
  {
    diaString = "0" + String(dia);
  }
  else
  {
    diaString = String(dia);
  }

  if (hora < 10)
  {
    horaString = "0" + String(hora);
  }
  else
  {
    horaString = String(hora);
  }

  if (minuto < 10)
  {
    minutoString = "0" + String(minuto);
  }
  else
  {
    minutoString = String(minuto);
  }

  if (segundo < 10)
  {
    segundoString = "0" + String(segundo);
  }
  else
  {
    segundoString = String(segundo);
  }
  if (ano < 2020)
  {
    dateTime = "";
  }
  else
  {
    dateTime = String(ano) + "-" + mesString + "-" + diaString + " " + horaString + ":" + minutoString + ":" + segundoString;
  }

  return dateTime;
}
void Adjust_date_time(String dateTime)
{ // 2020-07-01 19:00:00

  String ano = dateTime.substring(0, 4);
  String mes = dateTime.substring(5, 7);
  String dia = dateTime.substring(8, 10);
  String hora = dateTime.substring(11, 13);
  String minuto = dateTime.substring(14, 16);
  String segundo = dateTime.substring(17, 19);

  DateTime now = DateTime(ano.toInt(), mes.toInt(), dia.toInt(), hora.toInt(), minuto.toInt(), segundo.toInt());
  rtc.adjust(now);
}

void Init_wifi()
{
  wifi.PwrON();
  wifiActivado = wifi.Status();
}

void Stop_wifi()
{
  wifi.PwrOFF();
  wifiActivado = wifi.Status();
}

void Sleep_now()
{
  wdt.Disable();
  Stop_wifi();
  Stop_rfid();
  Stop_gps();

  digitalWrite(PWR_AUDIO, LOW); // Apago amplificador de audio

  Activar_indicador(_batStatusIndicator, "Apagado");

  Serial.println("Dormido...");
  PM->SLEEPCFG.bit.SLEEPMODE = 0x4; // Standby sleep mode
  while (PM->SLEEPCFG.bit.SLEEPMODE != 0x4)
    ;                               // Wait for it to take
  USB->DEVICE.CTRLA.bit.ENABLE = 0; // Shutdown the USB peripheral
  while (USB->DEVICE.SYNCBUSY.bit.ENABLE)
    ; // Wait for synchronization
  __WFI();
}

static float Convert_dec_to_frac(uint8_t val)
{
  float float_val = (float)val;
  if (val < 10)
  {
    return (float_val / 10.0);
  }
  else if (val < 100)
  {
    return (float_val / 100.0);
  }
  else
  {
    return (float_val / 1000.0);
  }
}

static float Calculate_temperature(uint16_t TP, uint16_t TC)
{
  uint32_t TLI = (*(uint32_t *)FUSES_ROOM_TEMP_VAL_INT_ADDR & FUSES_ROOM_TEMP_VAL_INT_Msk) >> FUSES_ROOM_TEMP_VAL_INT_Pos;
  uint32_t TLD = (*(uint32_t *)FUSES_ROOM_TEMP_VAL_DEC_ADDR & FUSES_ROOM_TEMP_VAL_DEC_Msk) >> FUSES_ROOM_TEMP_VAL_DEC_Pos;
  float TL = TLI + Convert_dec_to_frac(TLD);

  uint32_t THI = (*(uint32_t *)FUSES_HOT_TEMP_VAL_INT_ADDR & FUSES_HOT_TEMP_VAL_INT_Msk) >> FUSES_HOT_TEMP_VAL_INT_Pos;
  uint32_t THD = (*(uint32_t *)FUSES_HOT_TEMP_VAL_DEC_ADDR & FUSES_HOT_TEMP_VAL_DEC_Msk) >> FUSES_HOT_TEMP_VAL_DEC_Pos;
  float TH = THI + Convert_dec_to_frac(THD);

  uint16_t VPL = (*(uint32_t *)FUSES_ROOM_ADC_VAL_PTAT_ADDR & FUSES_ROOM_ADC_VAL_PTAT_Msk) >> FUSES_ROOM_ADC_VAL_PTAT_Pos;
  uint16_t VPH = (*(uint32_t *)FUSES_HOT_ADC_VAL_PTAT_ADDR & FUSES_HOT_ADC_VAL_PTAT_Msk) >> FUSES_HOT_ADC_VAL_PTAT_Pos;

  uint16_t VCL = (*(uint32_t *)FUSES_ROOM_ADC_VAL_CTAT_ADDR & FUSES_ROOM_ADC_VAL_CTAT_Msk) >> FUSES_ROOM_ADC_VAL_CTAT_Pos;
  uint16_t VCH = (*(uint32_t *)FUSES_HOT_ADC_VAL_CTAT_ADDR & FUSES_HOT_ADC_VAL_CTAT_Msk) >> FUSES_HOT_ADC_VAL_CTAT_Pos;

  // From SAMD51 datasheet: section 45.6.3.1 (page 1327).
  return (TL * VPH * TC - VPL * TH * TC - TL * VCH * TP + TH * VCL * TP) / (VCL * TP - VCH * TP - VPL * TC + VPH * TC);
}

float Get_tempc()
{
  // enable and read 2 ADC temp sensors, 12-bit res
  volatile uint16_t ptat;
  volatile uint16_t ctat;
  SUPC->VREF.reg |= SUPC_VREF_TSEN | SUPC_VREF_ONDEMAND;
  // ADC0->CTRLB.bit.RESSEL = ADC_CTRLB_RESSEL_12BIT_Val;//Se deja comentado para trabajar a 10BIT
  while (ADC0->SYNCBUSY.reg & ADC_SYNCBUSY_CTRLB)
    ; // wait for sync
  while (ADC0->SYNCBUSY.reg & ADC_SYNCBUSY_INPUTCTRL)
    ; // wait for sync
  ADC0->INPUTCTRL.bit.MUXPOS = ADC_INPUTCTRL_MUXPOS_PTAT;
  while (ADC0->SYNCBUSY.reg & ADC_SYNCBUSY_ENABLE)
    ;                            // wait for sync
  ADC0->CTRLA.bit.ENABLE = 0x01; // Enable ADC

  // Start conversion
  while (ADC0->SYNCBUSY.reg & ADC_SYNCBUSY_ENABLE)
    ; // wait for sync

  ADC0->SWTRIG.bit.START = 1;

  // Clear the Data Ready flag
  ADC0->INTFLAG.reg = ADC_INTFLAG_RESRDY;

  // Start conversion again, since The first conversion after the reference is changed must not be used.
  ADC0->SWTRIG.bit.START = 1;

  while (ADC0->INTFLAG.bit.RESRDY == 0)
    ; // Waiting for conversion to complete
  ptat = ADC0->RESULT.reg;

  while (ADC0->SYNCBUSY.reg & ADC_SYNCBUSY_INPUTCTRL)
    ; // wait for sync
  ADC0->INPUTCTRL.bit.MUXPOS = ADC_INPUTCTRL_MUXPOS_CTAT;
  // Start conversion
  while (ADC0->SYNCBUSY.reg & ADC_SYNCBUSY_ENABLE)
    ; // wait for sync

  ADC0->SWTRIG.bit.START = 1;

  // Clear the Data Ready flag
  ADC0->INTFLAG.reg = ADC_INTFLAG_RESRDY;

  // Start conversion again, since The first conversion after the reference is changed must not be used.
  ADC0->SWTRIG.bit.START = 1;

  while (ADC0->INTFLAG.bit.RESRDY == 0)
    ; // Waiting for conversion to complete
  ctat = ADC0->RESULT.reg;

  while (ADC0->SYNCBUSY.reg & ADC_SYNCBUSY_ENABLE)
    ;                            // wait for sync
  ADC0->CTRLA.bit.ENABLE = 0x00; // Disable ADC
  while (ADC0->SYNCBUSY.reg & ADC_SYNCBUSY_ENABLE)
    ; // wait for sync

  return Calculate_temperature(ptat, ctat);
}

bool Connect_to_wifi()
{
  if (JACO_DESCONECTADO)
  {
    Activar_indicador(_batStatusIndicator, "Apagado");
    wdt.Reset_device(100);
  }
  else
  {
    String cadena[sizeof(NAME_SECRET_FILE) / sizeof(NAME_SECRET_FILE[0])]; // vector para almacenar los datos de cada archivo wifi

    // Extraemos de la memoria las redes wifi descargadas y las agregadas por el usuario
    for (int i = 0; i < sizeof(NAME_SECRET_FILE) / sizeof(NAME_SECRET_FILE[0]); i++)
    {
      cadena[i] = storage.Read_write_document(NAME_SECRET_FILE[i], 'r', false, "");
    }

    String fileWifi[3] = {cadena[0], cadena[1], SOPORT_WIFI}; // Creamos un vector para almacenar (wifi descargados, wifi agregado por el usuario, wifi soporte)

    myTools.Print("file[0]: ", debugEnable);
    myTools.Println(fileWifi[0], debugEnable);
    myTools.Print("file[1]: ", debugEnable);
    myTools.Println(fileWifi[1], debugEnable);
    myTools.Print("file[2]: ", debugEnable);
    myTools.Println(fileWifi[2], debugEnable);

    wdt.Disable();
    bool validacion = wifi.Connect(fileWifi, sizeof(fileWifi) / sizeof(fileWifi[0]));
    wdt.Enable();

    return validacion;
  }
}
bool Guardar_datos(String *datos, unsigned char tamanoDatos)
{
  return storage.Guardar_datos(datos, tamanoDatos);
}

bool Guardar_datos(String *datos, String file, unsigned char tamanoDatos)
{
  return storage.Guardar_datos(datos, file, tamanoDatos);
}

void Set_personaID(unsigned long id)
{
  if (id != myJaco->GetPersonaID(myJaco))
  {
    Save_cierre_Dia(datosGps[2], myJaco->GetPersonaID(myJaco));
    storage.Set_persona_id(id);
  }
}

void Set_file_tipo(fileTipo selfTipo)
{
  _myFileTipo = selfTipo;
}

void Activar_alarma(unsigned char tipoAlarma, unsigned long timeInterval)
{
  if (timeInterval > 0)
  {
    switch (tipoAlarma)
    {
      case _alarmaLabor:
        alarmaLabor.Enable(timeInterval);
        alarmaPersona.Disable();
        alarmaGps.Disable();
        alarmaSync.Disable();
        alarmaMemoria.Disable();
        break;
      case _alarmaPersona:
        alarmaLabor.Disable();
        alarmaPersona.Enable(timeInterval);
        alarmaGps.Disable();
        alarmaSync.Disable();
        alarmaMemoria.Disable();
        break;
      case _alarmaGps:
        alarmaLabor.Disable();
        alarmaPersona.Disable();
        alarmaGps.Enable(timeInterval);
        alarmaSync.Disable();
        alarmaMemoria.Disable();
        break;
      case _alarmaSync:
        alarmaLabor.Disable();
        alarmaPersona.Disable();
        alarmaGps.Disable();
        alarmaSync.Enable(timeInterval);
        alarmaMemoria.Disable();
        break;
      case _alarmaMemoria:
        alarmaLabor.Disable();
        alarmaPersona.Disable();
        alarmaGps.Disable();
        alarmaSync.Disable();
        alarmaMemoria.Enable(timeInterval);
        break;
      default:
        break;
    }
  }
  else
  {
    switch (tipoAlarma)
    {
      case _alarmaLabor:
        alarmaLabor.Enable();
        break;
      case _alarmaPersona:
        alarmaPersona.Enable();
        break;
      case _alarmaGps:
        alarmaGps.Enable();
        break;
      case _alarmaSync:
        alarmaSync.Enable();
        break;
      case _alarmaMemoria:
        alarmaMemoria.Enable();
        break;
      default:
        break;
    }
  }
}

void Activar_alarma(unsigned char tipoAlarma)
{
  Activar_alarma(tipoAlarma, 0);
}

void Desactivar_alarmas()
{
  alarmaLabor.Disable();
  alarmaPersona.Disable();
  alarmaGps.Disable();
  alarmaSync.Disable();
  alarmaMemoria.Disable();
}

void Activar_indicador(unsigned char tipoIndicador, String color, unsigned long timeInterval)
{
  switch (tipoIndicador)
  {
    case _configModeIndicator:
      batStatusIndicator.Disable();
      gpsIndicator.Disable();
      configModeIndicator.Enable(color, timeInterval);
      break;
    case _gpsIndicator:
      batStatusIndicator.Disable();
      configModeIndicator.Disable();
      gpsIndicator.Enable(color, timeInterval);
      break;
    case _batStatusIndicator:
      configModeIndicator.Disable();
      gpsIndicator.Disable();
      batStatusIndicator.Enable(color, timeInterval);
      break;
    default:
      break;
  }
}

void Activar_indicador(unsigned char tipoIndicador, String color)
{
  Activar_indicador(tipoIndicador, color, 0);
}

void Activar_indicador(unsigned char tipoIndicador, unsigned long timeInterval)
{
  Activar_indicador(tipoIndicador, "", timeInterval);
}

void Activar_indicador(unsigned char tipoIndicador)
{
  Activar_indicador(tipoIndicador, "", 0);
}

void Handle_feedbacks()
{
  /*
    Handle alarmas de sonido
  */
  alarmaMemoria.Handle();
  alarmaSync.Handle();
  alarmaGps.Handle();
  alarmaPersona.Handle();
  alarmaLabor.Handle();

  /*
    Handle indicadores RGB
  */
  gpsIndicator.Handle();
  configModeIndicator.Handle();
  batStatusIndicator.Handle();
}

void Set_modules_debugger(bool state)
{
  rfid.Set_debug(state);
  gps.Set_debug(state);
  wifi.Set_debug(state);
  storage.Set_debug(state);
}

void Save_cierre_Dia (String _fecha, uint32_t _personaId)
{
  /*Solo se guarda cierre de dia si existe fecha y hora valida*/
  if (_fecha != "")
  {
    /*Variable locales*/
    uint16_t nCierres = Cierre_dia_counter ();
    String payloadToSave = "";

    /*Creamos el payload con los datos de cierre dia*/
    if (_personaId > 0 && myJaco->GetJacoID(myJaco) > 0) //Si existe personaId se hace cierre de dia con personaId y zanteId
    {
      payloadToSave += "[{\"persona_id\":\"";
      payloadToSave += (String)_personaId;
      payloadToSave += "\",\"zante_id\":\"";
      payloadToSave += (String)myJaco->GetJacoID(myJaco);
      payloadToSave += "\",\"fecha\":\"";
      payloadToSave += _fecha;
      payloadToSave += "\",\"version\":\"";
      payloadToSave += CODE_VERSION;
      payloadToSave += "\"}]";
    } else if (myJaco->GetJacoID(myJaco) > 0) //Si no existe personaId pero si zanteId se hace cierre de dia solo con zanteId
    {
      payloadToSave = "[{\"zante_id\":\"";
      payloadToSave += (String)myJaco->GetJacoID(myJaco);
      payloadToSave += "\",\"fecha\":\"";
      payloadToSave += _fecha;
      payloadToSave += "\",\"version\":\"";
      payloadToSave += CODE_VERSION;
      payloadToSave += "\"}]";
    } else
    {
      // El jaco no tiene asignado un Id (Esto no deberia pasar)
    }

    /*Guardamos el payloadToSave en la memoeria*/
    if (payloadToSave != "")
      storage.Read_write_document(ROOT_NAME_CIERRE_DIA + (String)nCierres + ".txt", 'w', false, payloadToSave);
  }
}

void Sync_cierre_dia ()
{
  /*Variable locales*/
  uint16_t nCierres = Cierre_dia_counter ();

  /*Verificamos si hay cierres de dia por sincronizar*/
  if (nCierres > 0)
  {
    /*Realizo un Request_to_server por cada cierre de dia encontrado*/
    for (uint16_t i = 0; i < nCierres; i++)
    {
      String payload = storage.Read_write_document(ROOT_NAME_CIERRE_DIA + (String)i + ".txt", 'r', false, "");
      Request_to_server(_myUrlTipo, payload);
      storage.Delete(ROOT_NAME_CIERRE_DIA + (String)i + ".txt");
    }
  }
}

uint16_t Cierre_dia_counter ()
{
  /*Variable locales*/
  uint16_t _nCierres = 0;

  /*Contar la cantidad de cierres de dia existentes (maximo 1000, debido al tamaño maximo de nombres posibles 0 -> 999)*/
  for (uint16_t i = 0; i < 1000; i++)
  {
    if (storage.Check_file_existence(ROOT_NAME_CIERRE_DIA + (String)i + ".txt"))
    {
      _nCierres ++;
    } else
    {
      break;
    }
  }

  return _nCierres;
}
