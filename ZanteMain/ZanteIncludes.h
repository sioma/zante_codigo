/*
 * ===============================================================================================
 * Name        : ZanteIncludes.h
 * Autor       : SIOMA, Diego Escobar
 * Version     : 5.1
 *  ===============================================================================================
 */

#ifndef ZanteIncludes_h
#define ZanteIncludes_h

/*
 * Se incluyen los archivos internos que contienen
 * las constantes, variable, definiciones y configuraciones
 */
#include "ZanteDefines.h"
#include "ZanteConstants.h"
#include "ZanteVariables.h"
#include "ZanteConfig.h"

/*
 * Se incluyen las librerias externas y las propias
 * que contienen las funciones importantes para
 * el funcionamiento del programa
 */
#include <Arduino.h>
#include <RTC_SAMD51.h>
#include <SPI.h>
#include <Wire.h>
#include <FlashStorage.h>
#include "wiring_private.h"
#include <ModuloGPS.h>
#include <Tools.h>
#include <Jaco_properties.h>
#include <ModuloSD.h>
#include <ModuloFeedback.h>
#include <SparkFun_MAX1704x_Fuel_Gauge_Arduino_Library.h>

/*
 * Para las PCB V1 y V3 se requieren distintas librerias
 */
#ifdef _JACO_V3_
#include <ModuloRFID_ID12LA.h>
#include <ModuloWiFi.h>
#else
#ifdef _JACO_V1_
#include <ModuloRFID_RDM630.h>
#include <ModuloWiFi_AT.h>
#endif
#endif

#endif /*ZanteIncludes_h*/
