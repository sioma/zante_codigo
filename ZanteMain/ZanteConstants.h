/*  ===============================================================================================
    Name        : ZanteConstants.h
    Autor       : SIOMA, Diego Escobar
    Version     : 5.1
    ===============================================================================================
*/

#ifndef ZanteConstants_h
#define ZanteConstants_h

unsigned long TIEMPO_ZANTEPS = 60000;
unsigned long TIEMPO_MUESTREO_GPS = 3000;
unsigned long TIEMPO_INTERRUPCION = 2300;
unsigned long TIEMPO_ALARMA_GPS = 15000;
unsigned long TIEMPO_ON = 500;
unsigned long TIEMPO_OFF_1S = 500;
unsigned long TIEMPO_OFF_5S = 4500;
unsigned long TIEMPO_FIN = 60000;
unsigned long TIEMPO_FIN_2 = 60000;
unsigned long DEBOUNCE_TIME_THRESHOLD = 500;

int V_HIGH = 818;


float TONO_ALARM_GPS[2] = {800.5, 200.2};
float TONO_TAG[2] = {659.26, 783.99};
float TONO_PERSONA[2] = {900, 500};

unsigned int TONO_ALARM_GPS_DELAY[2] = {10, 20};
unsigned int TONO_TAG_DELAY[2] = {50, 100};
unsigned int TONO_PERSONA_DELAY[2] = {50, 300};

unsigned long TRIGGER_TIME_ALARMA_GPS = 1000;

unsigned char TAMANO_TRAMA_COORDENADAS = 57;
unsigned char TAMANO_TRAMA_OPERACIONES = 65;
unsigned char TAMANO_TRAMA_PARAMETROS = 33;
unsigned char TAMANO_TRAMA_2LABORS = 76;

unsigned char PINES_RGB [3] = {RED_PIN, GREEN_PIN, BLUE_PIN};
unsigned int N_PINES = sizeof(PINES_RGB) / sizeof(PINES_RGB[0]);

String NAME_SECRET_FILE[2] = {"SECRETS", "SECRETS1"};
String USERMASTER_KEY = "S10m4Ec0";
String ROOT_NAME_CIERRE_DIA = "cldia";

String SOPORT_WIFI = "[{\"ssid\":\"SIOMA.APP\",\"psw\":\"20sioma20\"}]";

char *HEADERS_TRAMAS[10] = {"\"persona_id\"",
                            "\"lat\"",
                            "\"lng\"",
                            "\"fecha\"",
                            "\"tipo_labor_id\"",
                            "\"precision\"",
                            "\"voltaje\"",
                            "\"energia\"",
                            "\"temperatura\"",
                            "\"otros\""
                           };

char *FORMAT_DATA [10] = {"iiiiiiiiii",
                          "-iii.iiiiii",
                          "-iii.iiiiii",
                          "iiii-ii-ii ii:ii:ii",
                          "iiiiiiiiii",
                          "ii",
                          "i.ii",
                          "i",
                          "iii.ii",
                          "iiiiiiiiii"
                         };
unsigned char LEN_DATA [10] = {10,
                               11,
                               11,
                               19,
                               10,
                               2,
                               4,
                               1,
                               6,
                               10
                              };

String HEADERS[6] = {"zante-id", String(NULL), "Connection", "close", "Content-Type", "application/x-www-form-urlencoded"};
unsigned char SIZE_HEADERS = sizeof(HEADERS) / sizeof(HEADERS[0]);

char *FILE_NAMES_JACO[7] = {"idperson.txt",
                            "coordena.txt",
                            "zanteps.txt",
                            "operacio.txt",
                            "fingerp.txt",
                            "idZante.txt",
                            "dolabors.txt"
                           };

char *SERVER_JACO_PARAMETERS[11] = {"inventa.siomapp.com",
                                    "/2/zantefecha",
                                    "/2/wifis",
                                    "/2/cierre_dia_personas",
                                    "/2/operacions",
                                    "/2/coordenadas",
                                    "/2/zanteps",
                                    "/2/fingerprint/inventa.siomapp.com",
                                    "/2/Update",
                                    "/2/mac",
                                    "VAkbEyowa0Ae0gGfLUG/S0fJikDHEEdDEA9sKadU+x5ed7yOs8mnqrqbu+pTJ8CrMaCAa5WKDlY5xe1BHIWooA=="
                                   };

/*char *SERVER_JACO_PARAMETERS[11] = {"inventa.sioma.dev",
                                    "/2/zantefecha",
                                    "/2/wifis",
                                    "/2/cierre_dia_personas",
                                    "/2/operacions",
                                    "/2/coordenadas",
                                    "/2/zanteps",
                                    "/2/fingerprint/inventa.sioma.dev",
                                    "/2/Update",
                                    "/2/mac",
                                    "VAkbEyowa0Ae0gGfLUG/S0fJikDHEEdDEA9sKadU+x5ed7yOs8mnqrqbu+pTJ8CrMaCAa5WKDlY5xe1BHIWooA=="
                                   };*/


#endif /*ZanteConstants_h*/
