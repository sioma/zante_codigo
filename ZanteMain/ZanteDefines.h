/*  ===============================================================================================
 *  Name        : ZanteDefines.h
 *  Autor       : SIOMA, Diego Escobar
 *  Version     : 5.1
 *  ===============================================================================================
 */

#ifndef ZanteDefines_h
#define ZanteDefines_h

/*
 * Definicion de pines
 */
#define IRQ_PIN_RFID 41
#define IRQ_PIN_BOTON 35
#define CHARGER_PIN 20
#define SPEAKER_PIN 37
#define RED_PIN 3
#define GREEN_PIN 2
#define BLUE_PIN 9
#define PWR_GPS 42
#define CS_PIN_SD 12
#define BAT_PIN 21
#define PWR_WIFI 6
#define GPIO_WIFI 4
#define RESET_WIFI 18
#define PWR_AUDIO 43
#define PWR_5V 52

/*
 * Direccion base para log de sensor temperatura SAMD51
 */
#define NVMCTRL_TEMP_LOG NVMCTRL_TEMP_LOG_W0

/*
 * Tiempo default para reset por watchdog
 */
#define WDT_DEFAULT 5000

/*
 * Define cuando se considera que Jaco esta desconectado
 */
#define JACO_DESCONECTADO analogRead(CHARGER_PIN) < V_HIGH
//#define JACO_DESCONECTADO    analogRead(CHARGER_PIN) > V_HIGH //Solo para coger gps mientras esta conectado para debug

/*
 * Define el baudrate para las comunicaciones seriales
 */
#define GPS_BAUDRATE 9600
#define RFID_BAUDRATE 9600
#ifdef _JACO_V3_
#define WIFI_BAURATE 9600
#else
#ifdef _JACO_V1_
#define WIFI_BAURATE 115200
#endif
#endif

#endif /*ZanteDefines_h*/
